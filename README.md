# Microutils

Javascript utility library with utilities similar to lodash but micro sized.

## Installation

The module is packaged for all module style.
The main one in for ES6 import.

### ES6

For general import.

```javascript
import microutils from 'microutils.js';
```

For scoped import (better size)

```javascript
import difference from 'microutils.js/array/difference';
```

### CommonJS

```javascript
const microutils = require('microutils.js');
```

```javascript
const difference = require('microutils.js/array/difference.cjs');
```

### IIFE (Immediatly Invocked Function Env)

This for browser environment generally.
It will create a microutils global variable.

```javascript
<script src="./node_modules/microutils.js/index.iife.js"></script>
```
You can also import individual functions

```javascript
<script src="./node_modules/microutils.js/array/difference.iife.js"></script>
```

### UMD (Universal Module Definition)

see [ES6](#ES6)
see [CommonJS](#CommonJS)
