var clamp = (n, min, max) => {
  if (typeof n !== 'number') throw new TypeError(`${n} is not a number`);
  return Math.min(Math.max(n, min), max);
};

export { clamp as default };
