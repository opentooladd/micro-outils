this['/isArray'] = this['/isArray'] || {};
this['/isArray'].js = (function () {
	'use strict';

	var isArray = val => val instanceof Array;

	return isArray;

}());
