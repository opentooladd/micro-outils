this['/isObject'] = this['/isObject'] || {};
this['/isObject'].js = (function () {
	'use strict';

	var isObject = val => (typeof val === 'object' && !(val instanceof Function) && !(val instanceof Array));

	return isObject;

}());
