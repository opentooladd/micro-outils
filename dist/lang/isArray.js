var isArray = val => val instanceof Array;

export default isArray;
