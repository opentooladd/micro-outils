(function (global, factory) {
	typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() :
	typeof define === 'function' && define.amd ? define(factory) :
	(global['/isObject'] = global['/isObject'] || {}, global['/isObject'].js = factory());
}(this, (function () { 'use strict';

	var isObject = val => (typeof val === 'object' && !(val instanceof Function) && !(val instanceof Array));

	return isObject;

})));
