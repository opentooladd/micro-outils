export default ([
    {
        o1: {
            o1test1: 'o1test1',
            o2: {
                o2test1: 'test1',
                o2test2: [0, 1, 2],
            },
        },
    },
    {
        o1: {
            o1test1: 'test1',
            o2: {
                o2test1: 'test1',
                o2test2: [0, 1, 2],
                o1: {
                    o1test1: 'test1',
                    o3: {
                        o3test1: 'test1',
                        o3test2: [0, 1, 2],
                    },
                },
            },
        },
    }
]);
