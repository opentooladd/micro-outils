import closest from './array/closest';
import difference from './array/difference';
import findIndex from './array/findIndex';
import groupBy from './array/groupBy';
import regexp from './constants/regexp';
import serialize from './functions/serialize';
import unserialize from './functions/unserialize';
import deepCopy from './lang/deepCopy';
import get from './lang/get';
import isArray from './lang/isArray';
import isObject from './lang/isObject';
import merge from './lang/merge';
import parseBooleans from './lang/parseBooleans';
import set from './lang/set';
import uuid from './lang/uuid';
import clamp from './numbers/clamp';
import getNumFromString from './numbers/getNumFromString';
import getUnitFromString from './numbers/getUnitFromString';

const modules = {
  closest,
  difference,
  findIndex,
  groupBy,
  regexp,
  serialize,
  unserialize,
  deepCopy,
  get,
  isArray,
  isObject,
  merge,
  parseBooleans,
  set,
  uuid,
  clamp,
  getNumFromString,
  getUnitFromString,
}
export default modules;