import merge from './merge.js';

import { flows1, flows2 } from '../../tests/__fixtures__/flows';
import { testObject1, testObject2, testObjectResult } from '../../tests/__fixtures__/testObjects';
import flowResults from '../../tests/__fixtures__/flows_result';

describe('lang/merge', () => {
  it('should throw an error if trying to merge other thing than arrays or objects', () => {
    expect(() => merge('test', {})).toThrow(TypeError, 'only arrays and objects are mergeable');
    expect(() => merge(10, {})).toThrow(TypeError, 'only arrays and objects are mergeable');
    expect(() => merge(true, {})).toThrow(TypeError, 'only arrays and objects are mergeable');
    expect(() => merge(() => {}, {})).toThrow(TypeError, 'only arrays and objects are mergeable');
    //
    expect(() => merge([], 'test')).toThrow(TypeError, 'only arrays and objects are mergeable');
    expect(() => merge([], 10)).toThrow(TypeError, 'only arrays and objects are mergeable');
    expect(() => merge([], true)).toThrow(TypeError, 'only arrays and objects are mergeable');
    expect(() => merge([], () => {})).toThrow(TypeError, 'only arrays and objects are mergeable');
    expect(() => merge([], {}, 'test')).toThrow(new TypeError('obj and source[0] must be of same type'));
    expect(() => merge({}, {}, [], 10, 'test')).toThrow(TypeError, 'obj and source[1] must be of same type');
    expect(() => merge({}, {}, {}, 10, 'test', {})).toThrow(TypeError, 'obj and source[2] must be of same type');

    expect(() => merge({}, {})).not.toThrow();
    expect(() => merge([], [])).not.toThrow();
  });

  it('should merge an object and add keys if they don\'t already exist', () => {
    const testObj = {
      name: 'test string',
      list: [1, 2],
    };
    const testSrc = {
      name: 'test merge',
      list: [3, 4],
      num: 42,
      bool: true,
    };
    const result = {
      name: 'test merge',
      list: [3, 4],
      num: 42,
      bool: true,
    };
    expect(merge(testObj, testSrc)).toEqual(result);
  });

  it('should recursively merge objects', () => {
    expect(merge(flows1, flows2)).toEqual(flowResults);
    expect(merge(testObject1, testObject2)).toEqual(testObjectResult);
  });

  it('should merge an array', () => {
    expect(merge([], [10, 2, 10])).toEqual([10, 2, 10]);
  });
});
