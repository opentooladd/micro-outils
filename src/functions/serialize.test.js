import serialize from './serialize';

describe('libs/serialize', () => {
  test('should serialize a function', () => {
    const unserialized = {
      args: ['test'],
      body: `
      return test;
    `,
    };

    const f = serialize(function logMe(test) { return test });
    expect(f).toEqual(unserialized);
  });

  [[], {}, '', 10].forEach((v) => {
    test(`should throw an error if args is ${v}`, () => {
      expect(() => serialize(v)).toThrowError(TypeError('serialize: argument should be a function'));
    });
  });
});
