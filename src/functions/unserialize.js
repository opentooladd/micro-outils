import { COMMENT_REG_EXP, NEW_LINE_REG_EXP, BACKTICK_REG_EXP, COMMENTS_AND_BACKTICK } from '../constants/regexp';

export default (serialized) => {
  const args = serialized.args.map(el => el.replace(COMMENT_REG_EXP, '').replace(COMMENTS_AND_BACKTICK, '').replace(NEW_LINE_REG_EXP, '').replace(BACKTICK_REG_EXP, ''));
  const { body } = serialized;
  let func = new Function();
  try {
    func = new Function(args, body);
  } catch (e) {
    console.error(e);
  }
  return func;
};
