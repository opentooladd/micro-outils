export const COMMENT_REG_EXP = new RegExp(/\n+|(\/\*\*\/\n)+/, 'g');
export const NEW_LINE_REG_EXP = new RegExp(/^(\n+|\t+|\t\n+)(?!\w)$/, 'gm');
export const BACKTICK_REG_EXP = new RegExp(/`/, 'gm');
export const COMMENTS_AND_BACKTICK = new RegExp(/(\/\*``\*\/)+/, 'g');

export default ({
  COMMENT_REG_EXP,
  NEW_LINE_REG_EXP,
  BACKTICK_REG_EXP,
  COMMENTS_AND_BACKTICK,
});
