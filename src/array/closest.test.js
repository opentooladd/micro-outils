import closest from './closest';

describe('array/closest', () => {
  const testInputs = [-5, -2, 1, 5, 10, 200, 256, 501, 750, 751, 1004];
  const test = [-10, 0, 10, 500, 1000];
  const testResults = [-10, 0, 0, 0, 10, 10, 500, 500, 500, 1000, 1000];

  testInputs.forEach((input, index) => {
    it(`should return ${testResults[index]} for input ${input} on ${JSON.stringify(test)}`, () => {
      expect(closest(test, input)).toEqual(testResults[index]);
    });
  })
});