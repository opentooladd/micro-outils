import findIndex from './findIndex';

describe('array/findIndex', () => {
  const testInputs = [(i) => i === -10, (i) => i === 0, (i) => i === 10, (i) => i === 500, (i) => i === 1000, (i) => i === 31];
  const test = [-10, 0, 10, 500, 1000];
  const testResults = [0, 1, 2, 3, 4, -1];

  testInputs.forEach((input, index) => {
    it(`should return ${testResults[index]} for input ${input} on ${JSON.stringify(test)}`, () => {
      expect(findIndex(test, input)).toEqual(testResults[index]);
    });
  });

  it('should throw error if predicate is not a function', () => {
    expect(() => findIndex([10], 'test')).toThrow(TypeError('predicate is not a function'));
  });
});