import groupBy from './groupBy';

import columnPerStep from '../../tests/__fixtures__/columnsPerStep';
import columnPerStepResult from '../../tests/__fixtures__/columnPerStep_groupBy_result';

describe('array/groupBy', () => {
  it('should throw an error if first argument is not an array', () => {
    expect(() => groupBy({}, 'test')).toThrow(TypeError, '{} must be an array');
    expect(() => groupBy('test', 'test')).toThrow(TypeError, '\'test\' must be an array');
    expect(() => groupBy(42, 'test')).toThrow(TypeError, '42 must be an array');
    expect(() => groupBy(true, 'test')).toThrow(TypeError, 'true must be an array');
    expect(() => groupBy([], 'test')).not.toThrow();
  });

  it('should throw an error if iteratee is not defined', () => {
    expect(() => groupBy([])).toThrow(Error, 'iteratee must be defined, else it is just tje base array');
  });

  it('should group value by specified key', () => {
    expect(groupBy(columnPerStep, 'tableId')).toEqual(columnPerStepResult);
  });

});
