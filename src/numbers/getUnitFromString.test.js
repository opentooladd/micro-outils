import getUnitFromString from './getUnitFromString';

describe('getUnitFromString', () => {
  const units = ['%', 'px', 'vh', 'vw', 'em', 'deg'];
  units.forEach((unit) => {
    test(`should return ${unit} from '17${unit}'`, () => {
      expect(getUnitFromString(`17${unit}`)).toEqual(unit);
    });
  });
  test("should return '' from '17tata'", () => {
    expect(getUnitFromString('17tata')).toEqual('');
  });
  test("should return '' from ''", () => {
    expect(getUnitFromString('')).toEqual('');
  });
  test("should return '' from 10", () => {
    expect(getUnitFromString(10)).toEqual('');
  });
});