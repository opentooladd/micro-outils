import clamp from './clamp';

describe('numbers/clamp', () => {
  const tests = [
    {
      min: 0,
      max: 100,
      values: [10, 20, 50, 101, -1, 100],
      results: [10, 20, 50, 100, 0, 100],
    },
    {
      min: -5,
      max: 10,
      values: [10, 20, 50, 1, -1, -6, -5],
      results: [10, 10, 10, 1, -1, -5, -5],
    },
  ];
  const errors = [null, undefined, 'test', '10', {}, []];

  tests.forEach(t => test(`should clamp value between ${t.min} and ${t.max}`, () => {
    t.values.forEach((v, i) => expect(clamp(v, t.min, t.max)).toEqual(t.results[i]));
  }));

  test('should throw for if value is not a number', () => {
    errors.forEach(v => expect(() => clamp(v, 0, 10)).toThrowError(`${v} is not a number`));
  });
});
