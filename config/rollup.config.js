import { nodeResolve } from '@rollup/plugin-node-resolve';
import commonjs from '@rollup/plugin-commonjs';
import { globbySync } from 'globby';

const baseConfigs = globbySync('src/**/*.js').filter(inputFile => !inputFile.includes('test')).map(inputFile => ({
  input: inputFile,
  output: {
    name: inputFile.slice((inputFile.lastIndexOf('/') || 0)),
    file: inputFile.replace('src', 'dist'),
    format: 'es',
  },
  plugins: [
    nodeResolve(),
    commonjs({
      include: 'node_modules/**',
    }),
  ],
}));

const umdConfigs = globbySync('src/**/*.js').filter(inputFile => !inputFile.includes('test')).map(inputFile => ({
  input: inputFile,
  output: {
    name: inputFile.slice((inputFile.lastIndexOf('/') || 0)),
    file: inputFile.replace('.js', '.umd.js').replace('src', 'dist'),
    format: 'umd',
  },
  plugins: [
    nodeResolve(),
    commonjs({
      include: 'node_modules/**',
    }),
  ],
}));

const iifeConfigs = globbySync('src/**/*.js').filter(inputFile => !inputFile.includes('test')).map(inputFile => ({
  input: inputFile,
  output: {
    name: inputFile.slice((inputFile.lastIndexOf('/') || 0)),
    file: inputFile.replace('.js', '.iife.js').replace('src', 'dist'),
    format: 'iife',
  },
  plugins: [
    nodeResolve(),
    commonjs({
      include: 'node_modules/**',
    }),
  ],
}));

const cjsConfigs = globbySync('src/**/*.js').filter(inputFile => !inputFile.includes('test')).map(inputFile => ({
  input: inputFile,
  output: {
    name: inputFile.slice((inputFile.lastIndexOf('/') || 0)),
    file: inputFile.replace('.js', '.cjs.js').replace('src', 'dist'),
    format: 'cjs',
  },
  plugins: [
    nodeResolve(),
    commonjs({
      include: 'node_modules/**',
    }),
  ],
}));

export default [
  ...baseConfigs,
  ...umdConfigs,
  ...iifeConfigs,
  ...cjsConfigs,
];